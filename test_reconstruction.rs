use std::io::{self,Write};
use std::{thread,time};
use std::clone::Clone;
use std::time::Instant;
//7129,7127,7151,7159,7121
//2^{-1}= (7129+1)/2 mod 7129 = 3565
//2^{-1}= (7127+1)/2 mod 7127 = 3564
//2^{-1}= (7151+1)/2 mod 7151 = 3576
//2^{-1}= (7159+1)/2 mod 7159 = 3580
//2^{-1}= (7121+1)/2 mod 7121 = 3561

const inv_2_P1:u32=3565;
const inv_2_P2:u32=3564;
const inv_2_P3:u32=3576;
const inv_2_P4:u32=3580;
const inv_2_P5:u32=3561;

//2^{-1}= (N+1)/2 mod N = 9261162489424000044

//Found the inverse with the euclidean algorithm

const P1:u32=7129; //first modulus
const P2:u32=7127; //second modulus
const P3:u32=7151; //third modulus
const P4:u32=7159; //fourth modulus
const P5:u32=7121; //fifth modulus
const N:u128= (P1 as u128)*(P2 as u128)*(P3 as u128)*(P4 as u128)*(P5 as u128);
const Inv1:u32=534; //inverse of P2*P3*P4*P5 mod P1
const Inv2:u32=4732;//inverse of P1*P3*P4*P5 mod P2
const Inv3:u32=4277;//inverse of P1*P2*P4*P5 mod P3
const Inv4:u32=4234;//inverse of P1*P2*P3*P5 mod P4
const Inv5:u32=510;//inverse of P1*P2*P3*P4 mod P5


#[derive(Copy, Clone)]
pub struct DoubleInteger { 
    r_1: u32,
    r_2: u32,
    r_3: u32,
    r_4: u32,
    r_5: u32,
     
}

//constructors

impl DoubleInteger{
    pub fn new(r_1: u32,r_2: u32,r_3: u32,r_4: u32,r_5: u32) -> Self {
        Self { r_1,r_2,r_3,r_4,r_5 }
    }
}

impl DoubleInteger{
    pub fn new_from_doubleInteger(x:DoubleInteger) -> Self {
        let r_1=x.r_1;
        let r_2=x.r_2;
        let r_3=x.r_3;
        let r_4=x.r_4;
        let r_5=x.r_5;
        Self { r_1,r_2,r_3,r_4,r_5 }
    }
}

impl DoubleInteger{
    pub fn new_from_u64(x:u64) -> Self {
        let u =decompose(x);
        let r_1=u.r_1;
        let r_2=u.r_2;
        let r_3=u.r_3;
        let r_4=u.r_4;
        let r_5=u.r_5;
        Self { r_1,r_2,r_3,r_4,r_5 }
    }
}

impl DoubleInteger{
    pub fn assign(r_1: u32,r_2: u32,r_3: u32,r_4: u32,r_5: u32) -> Self {
        let r_1=r_1;
        let r_2=r_2;
        let r_3=r_3;
        let r_4=r_4;
        let r_5=r_5;
        
        Self { r_1,r_2,r_3,r_4,r_5 }
    }
}


fn extendedEuclide(a:u32,b:u32)->(u32,u32,u32){
    // I used that algorithm https://stackoverflow.com/questions/67097428/is-it-possible-to-implement-the-extended-euclidean-algorithm-with-unsigned-machi
    // Not sure why u and v are always under the biggest value, I think that's the goal of u_1=b-u_1; and v_1=a-v_1; it should not overflow in any case because the modulus I use are quite small
    
    let mut q:u32=0;
    let mut r_temp:u32=0;
    let mut u_temp:u32=0;
    let mut v_temp:u32=0;
    let mut u_1:u32=1;
    let mut v_1:u32=0;
    let mut u_2:u32=0;
    let mut v_2:u32=1;
    let mut r_1:u32 =a; 
    let mut r_2:u32 =b; 
    let mut n:u32=0;
    while r_2 !=0{
        q=r_1/r_2;
        r_temp = r_1;
        u_temp = u_1;
        v_temp = v_1;
        r_1=r_2;
        u_1=u_2;
        v_1=v_2;
        if r_temp>=q*r_2{
            r_2=r_temp-q*r_2;    
        }else{
            r_2=q*r_2-r_temp;
        }
        u_2=u_temp+q*u_2;
        v_2=v_temp+q*v_2;
        n+=1;
    }
    if n%2==1{
        u_1=b-u_1;
    }else{
        v_1=a-v_1
    }
    return (r_1,u_1,v_1);
}

//modular inverse

fn inverseMod(n:u32,modu:u32)->u32{
    let mut inv:u32 =0;
    let result = extendedEuclide(modu,n);
    inv = result.2;
    return inv;
}

//addition

pub fn add(x:DoubleInteger,y:DoubleInteger)->DoubleInteger{
    let resultat =DoubleInteger::new((x.r_1+y.r_1)%P1,(x.r_2+y.r_2)%P2,(x.r_3+y.r_3)%P3,(x.r_4+y.r_4)%P4,(x.r_5+y.r_5)%P5);
    return resultat;
}

//subtraction

pub fn sub(x:DoubleInteger,y:DoubleInteger)->DoubleInteger{
    let mut r_1 =0;
    let mut r_2 =0;
    let mut r_3 =0;
    let mut r_4 =0;
    let mut r_5 =0;
    //compesating for the lack of sign
    //might go faster with a dedicated bitmask for each modulo
    if x.r_1>=y.r_1{
        r_1=x.r_1-y.r_1;
    }else{
        r_1=x.r_1+P1;
        r_1=r_1-y.r_1;
    }
    if x.r_2>=y.r_2{
        r_2=x.r_2-y.r_2;
    }else{
        r_2=x.r_2+P2;
        r_2=r_2-y.r_2;
    }
    if x.r_3>=y.r_3{
        r_3=x.r_3-y.r_3;
    }else{
        r_3=x.r_3+P3;
        r_3=r_3-y.r_3;
    }
    if x.r_4>=y.r_4{
        r_4=x.r_4-y.r_4;
    }else{
        r_4=x.r_4+P4;
        r_4=r_4-y.r_4;
    }
    if x.r_5>=y.r_5{
        r_5=x.r_5-y.r_5;
    }else{
        r_5=x.r_5+P5;
        r_5=r_5-y.r_5;
    }
    let resultat =DoubleInteger::new(r_1,r_2,r_3,r_4,r_5);
    let mut temp:u128 = (P2 as u128)*(P3 as u128)*(P4 as u128)*(P5 as u128)*(Inv1 as u128)*(r_1 as u128)+
     (P1 as u128)*(P3 as u128)*(P4 as u128)*(P5 as u128)*(Inv2 as u128)*(r_2 as u128)+
     (P1 as u128)*(P2 as u128)*(P4 as u128)*(P5 as u128)*(Inv3 as u128)*(r_3 as u128)+
     (P1 as u128)*(P2 as u128)*(P3 as u128)*(P5 as u128)*(Inv4 as u128)*(r_4 as u128)+
     (P1 as u128)*(P2 as u128)*(P3 as u128)*(P4 as u128)*(Inv5 as u128)*(r_5 as u128);
    temp = temp%N;
    println!("sub_in: {}",temp);
    return resultat;
}

//multiplication

pub fn mul(x:DoubleInteger,y:DoubleInteger)->DoubleInteger{
    let resultat =DoubleInteger::new((x.r_1*y.r_1)%P1,(x.r_2*y.r_2)%P2,(x.r_3*y.r_3)%P3,(x.r_4*y.r_4)%P4,(x.r_5*y.r_5)%P5);
    return resultat;    
}

// modular exponentiation
pub fn pow(x:DoubleInteger,exp:u32)->DoubleInteger{
    //This is not exactly a fast exponentiation I use only the leading bit of the binary decomposition of the exponent;
    let mut temp = DoubleInteger::new(x.r_1,x.r_2,x.r_3,x.r_4,x.r_5);
    let mut res = DoubleInteger::new(1,1,1,1,1);
    let mut n=0;
    if exp==0{
        res = DoubleInteger::assign(1,1,1,1,1);
        return res;
    }else{
        let mut t = exp;
        if t&1==1{
            res.r_1=temp.r_1;
            res.r_2=temp.r_2;
            res.r_3=temp.r_3;
            res.r_4=temp.r_4;
            res.r_5=temp.r_5;    
        }
        t=t>>1;
        while t!=0{
            temp=mul(temp,temp);
            println!("{}",temp.r_1);
            if t&1==1{
                res.r_1=res.r_1*temp.r_1;
                res.r_2=res.r_2*temp.r_2;
                res.r_3=res.r_3*temp.r_3;
                res.r_4=res.r_4*temp.r_4;
                res.r_5=res.r_5*temp.r_5;    
            }
            t=t>>1;
        }
        return res;
    }
}

// shift left, works properly
pub fn leftbitshift(x:DoubleInteger, t:u32 )->DoubleInteger{
    let mut y = DoubleInteger::new(2,2,2,2,2);
    y=pow(y,t);
    let r=mul(x,y);
    return r;
}

//shift right, doesn't work except on special occasions

pub fn rightbitshift(x:DoubleInteger, t:u32 )->DoubleInteger{
    //I applied an algorithm described in https://core.ac.uk/download/pdf/82201912.pdf which is close to what I had in mind. 
    //which is not supposed to work for the number system I took, but it appears that sometimes it works, unfortunately not all the time.
    //It seems it's quite a hard problem in general
    //I will look at it further, I never implemented the RNS previously, it's quite interesting.
    /* 
    let mut y = DoubleInteger::new(2,2,2,2,2);
    y=pow(y,t);
    
    // compute de modular inverse of 2^t mod Pi
    
    let inv_1= (inverseMod(y.r_1,P1))%P1;
    let inv_2= (inverseMod(y.r_2,P2))%P2;
    let inv_3= (inverseMod(y.r_3,P3))%P3;
    let inv_4= (inverseMod(y.r_4,P4))%P4;
    let inv_5= (inverseMod(y.r_5,P5))%P5;
    
    // say m= 2^t+r mod N, then (2^t)^(-1)*mi = q+ (2^t)^(-1)*r mod N, (2^t)^(-1)*mi -(2^t)^(-1)*(m%2^t) = q mod N 
    // Now the question is how to do the modulo 2^t inside a modulo m_i?  
     
    let res_1 = ((x.r_1-x.r_1%y.r_1)*inv_1)%P1;((x.r_5-x.r_5%y.r_5)*inv_5)%P5;
    let res_2 = ((x.r_2-x.r_2%y.r_2)*inv_2)%P2;
    let res_3 = ((x.r_3-x.r_3%y.r_3)*inv_3)%P3;
    let res_4 = ((x.r_4-x.r_4%y.r_4)*inv_4)%P4;
    let res_5 = ((x.r_5-x.r_5%y.r_5)*inv_5)%P5;
    //2^{-1}= (7129+1)/2 mod 7129 = 3565
    //2^{-1}= (7127+1)/2 mod 7127 = 3564
    //2^{-1}= (7151+1)/2 mod 7151 = 3576
    //2^{-1}= (7121+1)/2 mod 7121 = 3561
    */

    let mut y = DoubleInteger::new(2,2,2,2,2);
    y=pow(y,t);
    
    let mut res_1 = 0;
    let mut res_2 = 0;
    let mut res_3 = 0;
    let mut res_4 = 0;
    let mut res_5 = 0;
    for r1 in 0..P1{
        if r1 <= x.r_1 && (x.r_1-r1)%y.r_1==0{
            res_1=(x.r_1-r1)/y.r_1;
        }else if r1 > x.r_1 && (x.r_1+P1-r1)%y.r_1==0 
        {
            res_1=(x.r_1+P1-r1)/y.r_1;
        }
    }
    for r2 in 0..P2{
        if r2 <= x.r_2 && (x.r_2-r2)%y.r_2==0{
            res_2=(x.r_2-r2)/y.r_2;
        }else if r2 > x.r_2 && (x.r_2+P2-r2)%y.r_2==0 
        {
            res_2=(x.r_2+P2-r2)/y.r_2;
        }
    }
    for r3 in 0..P3{
        if r3 <= x.r_3 && (x.r_3-r3)%y.r_3==0{
            res_3=(x.r_3-r3)/y.r_3;
        }else if r3 > x.r_3 && (x.r_3+P3-r3)%y.r_3==0 
        {
            res_3=(x.r_3+P3-r3)/y.r_3;
        }
    }
    for r4 in 0..P4{
        if r4 <= x.r_4 && (x.r_4-r4)%y.r_4==0{
            res_4=(x.r_4-r4)/y.r_4;
        }else if r4 > x.r_4 && (x.r_4+P4-r4)%y.r_4==0 
        {
            res_4=(x.r_4+P4-r4)/y.r_4;
        }
    }
    for r5 in 0..P5{
        if r5 <= x.r_5 && (x.r_5-r5)%y.r_5==0{
            res_5=(x.r_5-r5)/y.r_5;
        }else if r5 > x.r_5 && (x.r_5+P5-r5)%y.r_5==0 
        {
            res_5=(x.r_5+P5-r5)/y.r_5;
        }
    }
    
    let res = DoubleInteger::new(res_1%P1,res_2%P2,res_3%P3,res_4%P4,res_5%P5);
    
    return res;
    
    
    
    
    
    //results
    
    /* 
    //rem of n = q*2^t+rem
    let mut rem_1 = 0;
    let mut rem_2 = 0;
    let mut rem_3 = 0;
    let mut rem_4 = 0;
    let mut rem_5 = 0;

    // factor
    let mut m1:u32=0;
    let mut m2:u32=0;
    let mut m3:u32=0;
    let mut m4:u32=0;
    let mut m5:u32=0;

    for m1 in 1..P1{
        for s in 1..P1{
            if (m1*y.r_1)%P1==(x.r_1*s)%P1{
                let inv_s = inverseMod(s,P1);
                rem_1 = inv_s*((x.r_1*s)%(y.r_1*s))%P1;
                if x.r_1>=rem_1{
                    res_1 = (x.r_1-rem_1)%y.r_1;
                }else{
                    res_1 = (x.r_1+P1-rem_1)%y.r_1;
                }
                
            }        
        }   
    }
    for m2 in 1..P2{
        for s in 1..P2{
            if (m2*y.r_2)%P2==(x.r_2*s)%P2{
                let inv_s = inverseMod(s,P2);
                rem_2 = inv_s*((x.r_2*s)%(y.r_2*s))%P2;
                if x.r_2>=rem_2{
                    res_2 = (x.r_2-rem_2)%y.r_2;
                }else{
                    res_2 = (x.r_2+P2-rem_2)%y.r_2;
                }
            }        
        }   
    }
    for m3 in 1..P3{
        for s in 1..P3{
            if (m3*y.r_3)%P3==(x.r_3*s)%P3{
                let inv_s = inverseMod(s,P3);
                rem_3 = inv_s*((x.r_3*s)%(y.r_3*s))%P3;
                if x.r_3>=rem_3{
                    res_3 = (x.r_3-rem_3)%y.r_3;
                }else{
                    res_3 = (x.r_3+P3-rem_3)%y.r_3;
                }
            }        
        }   
    }
    for m4 in 1..P4{
        for s in 1..P4{
            if (m4*y.r_4)%P4==(x.r_4*s)%P4{
                let inv_s = inverseMod(s,P4);
                rem_4 = inv_s*((x.r_4*s)%(y.r_4*s))%P4;
                if x.r_4>=rem_4{
                    res_4 = (x.r_4-rem_4)%y.r_4;
                }else{
                    res_4 = (x.r_4+P4-rem_4)%y.r_4;
                }
            }        
        }   
    }
    for m5 in 1..P5{
        for s in 1..P5{
            if (m5*y.r_5)%P5==(x.r_5*s)%P5{
                let inv_s = inverseMod(s,P5);
                rem_5 = inv_s*((x.r_5*s)%(y.r_5*s))%P5;
                if x.r_5>=rem_5{
                    res_5 = (x.r_5-rem_5)%y.r_5;
                }else{
                    res_5 = (x.r_5+P5-rem_5)%y.r_5;
                }
            }        
        }   
    }
    // */
    
}


//probably not the most beautiful way to reconstruct a number

pub fn reconstruct(x:DoubleInteger)->u64{
    let p1= P1 as u128;
    let p2= P2 as u128;
    let p3= P3 as u128;
    let p4= P4 as u128;
    let p5= P5 as u128;
    
    let fact_1 =((x.r_1 as u128)*Inv1 as u128)%p1;
    let fact_2 =((x.r_2 as u128)*Inv2 as u128)%p2;
    let fact_3 =((x.r_3 as u128)*Inv3 as u128)%p3;
    let fact_4 =((x.r_4 as u128)*Inv4 as u128)%p4;
    let fact_5 =((x.r_5 as u128)*Inv5 as u128)%p5;
    let product = p1*p2*p3*p4*p5;
    
    let mut r= p3*p4*p5*(p2*fact_1+p1*fact_2)+p1*p2*(p5*(p4*fact_3+p3*fact_4)+p3*p4*fact_5);
    r=r%product;
    
    return r as u64;    
}

pub fn decompose(x:u64)->DoubleInteger{
    let r_1 = (x%(P1 as u64))as u32;
    let r_2 = (x%(P2 as u64))as u32;
    let r_3 = (x%(P3 as u64))as u32;
    let r_4 = (x%(P4 as u64))as u32;
    let r_5 = (x%(P5 as u64))as u32;
    
    let new_number= DoubleInteger::new(r_1,r_2,r_3,r_4,r_5);
    return new_number;    
}

fn main(){
    let x= 12105;
    let y= 3;
    let x_double=DoubleInteger::new_from_u64(x);
    let z = reconstruct(x_double);
    println!("{}",z);
    let y_double=DoubleInteger::new_from_u64(y);
    let z_2 = reconstruct(y_double);
    println!("{}",z_2);
    let start_time = Instant::now();
    let result_add = add(x_double,y_double);
    let elapsed_time = start_time.elapsed();
    println!("Elapsed time: {:?}", elapsed_time);
    let r_add = reconstruct(result_add);
    let start_time = Instant::now();
    let result_mul = mul(x_double,y_double);
    let elapsed_time = start_time.elapsed();
    println!("Elapsed time: {:?}", elapsed_time);
    let r_mul = reconstruct(result_mul);
    let start_time = Instant::now();
    let result_sub = sub(x_double,y_double);
    let elapsed_time = start_time.elapsed();
    println!("Elapsed time: {:?}", elapsed_time);
    let r_sub = reconstruct(result_sub);
    let start_time = Instant::now();
    let result_left_shift = leftbitshift(y_double,4);
    let elapsed_time = start_time.elapsed();
    println!("Elapsed time: {:?}", elapsed_time);
    let r_left_shift = reconstruct(result_left_shift);
    let result_right_shift = rightbitshift(x_double,1);
    let r_right_shift = reconstruct(result_right_shift);
    
    println!("r_add: {}",r_add);
    println!("r_mul: {}",r_mul);
    println!("r_sub: {}",r_sub);
    println!("r_left_shift: {}",r_left_shift);
    println!("r_right_shift: {}",r_right_shift);
    println!("N: {}",N);
    /* 
    for i in 2..7129{
        result= extendedEuclide(a,i);
        println!("i:{}",i);
        println!("r:{}",result.0);
        println!("u:{}",result.1);
        println!("v:{}",result.2);
    }
    // */
    
}