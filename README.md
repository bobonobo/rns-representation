# RNS representation

Ths is an attempt at creating a u64 from u32 numbers in rust using the RNS representation.
Underflow is carried out by checking parity, overflow is not implemented yet. Parity checking needs to be better implemented, for now it's a bit like looking at a LUT.

-Left shift is implemented by multiplying by 2. I might improve that very soon since the real left shift % Pi should work.

-Right shift is implemented using the parity check and multiplication by the modular inverse of two. 
i.e.

*$\lfloor \frac{m}{2} \rfloor \equiv 2^{-1} m (P_i)$ if $m$ is even and 

*$\lfloor \frac{m}{2} \rfloor \equiv 2^{-1} (m-1) (P_i)$ if $m$ is odd  

First implementation was a bit messy. It's still a bit messy but less meaningless/ more meaningful.


Too much copy, plenty of warning , will fix it soon.

